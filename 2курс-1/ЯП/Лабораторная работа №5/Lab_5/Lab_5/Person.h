#pragma once
#include <string>

class Person
{
public:
    virtual ~Person() = 0;               // ����������

    void add();                                   // ���������� ������� � ������

    void list_print();       // ����� ������ ��������

    virtual void show() = 0;

protected:
    std::string full_name;               // ��� �������

    static Person* begin;                // ����������� ��������� �� ������ ������
    Person* next_item = NULL;            // ��������� �� ��������� ������� ������

private:
};

inline Person::~Person() {}