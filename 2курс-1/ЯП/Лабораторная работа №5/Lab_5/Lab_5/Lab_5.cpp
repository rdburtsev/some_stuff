﻿#include <iostream>
#include "person.h"
#include "employee.h"
#include "worker.h"
#include "engineer.h"
#include "windows.h"

Person* Person::begin = NULL;

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    Employee A = Employee("Петр", "Инструктор по разбору печатных плат");
    Worker   B = Worker("Леонид");
    Engineer C = Engineer("Арсений");

    Person* M1 = &A;
    Person* M2 = &B;
    Person* M3 = &C;
    M1->show(); M2->show(); M3->show();
    printf("\n");

    M1->add(); M3->add();
    M1->list_print();
    printf("\n");

    M2->add();
    M1->list_print();
    printf("\n");

    return 0;
}
