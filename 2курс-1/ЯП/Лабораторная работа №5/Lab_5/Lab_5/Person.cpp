#include "Person.h"

void Person::add() {
    if (begin == NULL) {
        begin = this;
    }
    else {
        Person* ptr = begin;
        while (ptr->next_item != NULL) {
            ptr = ptr->next_item;
        }
        ptr->next_item = this;
    }
}

void Person::list_print() {
    if (begin == NULL) {
        printf("������ ����!\n");
    }
    else {
        Person* ptr = begin;
        int i = 1;
        while (ptr != NULL) {
            printf("%d. ", i);
            ptr->show();
            ptr = ptr->next_item;
            i++;
        }
    }
}