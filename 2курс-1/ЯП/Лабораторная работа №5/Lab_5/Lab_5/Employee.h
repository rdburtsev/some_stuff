#pragma once
#include "Person.h"
class Employee : public Person
{
public:
    Employee();
    Employee(std::string full_name, std::string post);
    virtual ~Employee();

    void show();

protected:
    std::string post;
private:
};