// Лабораторная работа №1, индивидуальное задание
// Бурцев Роман, КБ-211
// Подгруппа 1, вариант 2

/*
    Вариант 2.
    Класс Треугольник
    Свойства: три стороны
    Операции:
    - увеличение/уменьшение размера сторон на заданное количество процентов;
    - вычисление средней линии для любой из сторон;
    - определение вида треугольника по величине углов (Остроугольный, Тупоугольный,
      Прямоугольный);
    - определение значений углов.
*/

#define PI 3.14159265358979323846

#include <iostream>
#include <math.h>
#include <string>

class Triangle {
	
	public:
		// Конструктор
		Triangle();
		
		// Сеттер для сторон
		bool setSide(int numberOfSide, double length);
		
		// Геттер для сторон
		double getSide(int numberOfSide);
		
		// Вывод информации о длинах сторон
		void show();
		
		// Изменение размера всех сторон на заданное число процентов
		bool resize(double percent);
		
		// Вычисление средней линии для любой из сторон
		double middleLine(int numberOfSide);
		
		// Определение типа треугольника
		bool getType();
		
		// Определение величин углов
		double getAngle(int numberOfAngle);
		
	private:
		// Длины сторон
		double sides[3] = { 0 };
		
		// Квадрат числа для упрощения записи методов
		double sqr(double x);
};

int main() {
	setlocale(LC_ALL, "Russian");
	std::cout << "Программа для демонстрации работы класса Triangle\n";
	
	// Создание объекта класса Triangle
	Triangle tr;
	
	int answer;
	do {
        std::cout << "\nВыберите действие с треугольником (введите цифру варианта):\n";
        std::cout << "1. Увеличение/уменьшение размера сторон на заданное число процентов;\n";
        std::cout << "2. Вычисление средней линии любой из сторон;\n";
        std::cout << "3. Определение вида треугольника;\n";
        std::cout << "4. Определение значений углов;\n";
        std::cout << "5. Переопределение длин сторон;\n";
        std::cout << "6. Вывод длин сторон;\n";
        std::cout << "7. Завершение работы программы.\n>>>";
        std::cin >> answer;
        
        switch(answer) {
        case 1:
            {
                std::cout << "Введите число процентов для изменения стороны\n";
                std::cout << "Если нужно увеличить сторону, введите положительное число,\n";
                std::cout << "если нужно уменьшить - введите число со знаком '-'\n>>>";
                double percent;
                std::cin >> percent;
                tr.resize(percent);
            }
            break;
        case 2:
            {
                std::cout << "Введите номер стороны:\n>>>";
                int num;
                std::cin >> num;
                std::cout << "Средняя линия: " << tr.middleLine(num) << std::endl;
            }
            break;
        case 3:
			{
				tr.getType();
			}
			break;
        case 4:
            {
                for (int i = 1; i < 4; i++) {
                    std::cout << i << ". " << tr.getAngle(i) << " градусов\n";
                }
            }
            break;
        case 5:
            {
                tr = Triangle();
            }
            break;
        case 6:
            {
                tr.show();
            }
            break;
        case 7:
            break;
        default: 
            {
                std::cout << "Введено некорректное значение. Повторите попытку.\n";
            }
            break;
        }
    } while (answer != 7);
    std::cout << "\nЗавершение работы...\n";
    system("pause");
	
	return 0;
}

Triangle::Triangle() {
	std::cout << "Задайте исходные свойства треугольника\n";
    bool existence = false;
    do {
        for (int i = 0; i < 3; i++) {
            bool condition = false;
            while (!condition) {
                std::cout << "Введите длину cтороны " << i+1 << ":\n>>>";
                double value;
                std::cin >> value;
                condition = this -> setSide(i, value);
            }
            condition = false;
        }
        existence = this -> getType();
    } while (!existence);
}

// Сеттер для сторон
bool Triangle::setSide(int numberOfSide, double length) {
    if (length > 0) {
        sides[numberOfSide % 3] = length;
        return true;
    } else {
        std::cout << "Сторона не может иметь неположительную длину!\n";
        return false;
    }
}

// Геттер для сторон
double Triangle::getSide(int numberOfSide) {
	return sides[numberOfSide % 3];
}

// Вывод информации о длинах сторон
void Triangle::show() {
	printf("Треугольник имеет стороны длиной %0.2lf, %0.2lf, %0.2lf\n", sides[0], sides[1], sides[2]);
}

// Изменение размера всех сторон на заданное число процентов
bool Triangle::resize(double percent) {
	if (percent <= -100.0) {
		std::cout << "Невозможно выполнить операцию!\n";
		return false;
	}
	else {
		for (int i = 0; i < 3; i++) {
			sides[i] *= (100.0 + percent) * 0.01;
		}
		return true;
	}
}

// Вычисление средней линии для любой из сторон
double Triangle::middleLine(int numberOfSide) {
	return sides[ --numberOfSide % 3 ] / 2.0;
}

// Определение типа треугольника
bool Triangle::getType() {
		if ((sides[0] < sides[1] + sides[2]) &&
            (sides[1] < sides[0] + sides[2]) &&
            (sides[2] < sides[0] + sides[1]))
        {
            int max = 0, min = 0, middle;
            // Маленькая манипуляция с индексами сторон в массиве
            for (int i = 0; i < 3; i++) {
                if (sides[i] > sides[max])
                    max = i;
                if (sides[i] <= sides[min])
                    min = i;
            }
            middle = 3 - max - min;
            // Определение типа треугольника
            if (sqr(sides[max]) == sqr(sides[middle]) + sqr(sides[min])) {
                std::cout << "Треугольник прямоугольный!\n";
            }
            else if (sqr(sides[max]) > sqr(sides[middle]) + sqr(sides[min])) {
                std::cout << "Прямоугольник тупоугольный!\n";
            } else
                std::cout << "Прямоугольник остроугольный!\n";
            return true;
        }
        else {
            std::cout << "Треугольника с такими сторонами не существует!\n";
            return false;
        }
}

// Определение величин углов
double Triangle::getAngle(int numberOfAngle) {
	int n = --numberOfAngle;
	 // cos Alpha = (c^2 + b^2 - a^2) / 2cb
    return acos((sqr(sides[(n+2)%3]) + sqr(sides[(n+1)%3]) - sqr(sides[n%3])) /
		   (2*sides[(n+1)%3]*sides[(n+2)%3])) * 180.0 / PI;
}

// Квадрат числа
double Triangle::sqr(double x) {
	return x * x;
}
