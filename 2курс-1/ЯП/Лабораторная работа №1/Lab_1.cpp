// Лабораторная работа №1, общее задание
// Бурцев Роман, КБ-211
// Подгруппа 1, вариант 2

#include <iostream>
#include <string>
//#include <Windows.h>

// Определение класса Worker
class Worker {
    public:
        int age;
        std::string name;
        
        // Метод для печати информации о полях объекта
        void print_info() {
            std::cout << "Имя: " << name << ";\n";
            std::cout << "Возраст: " << age << ";\n";
        }
        
        // Метод для увеличения веса
        void eat(float how_much);
        
        // Метод-геттер для поля weight
        float get_weight();
        
        // Метод-сеттер для поля weight
        void set_weight(float new_weight);
        
        // Методы для изменения настроения
        void walk();
        void dance();
        void work();
        
        // Метод-геттер для настроения
        int get_mood();
        
    private:
        float weight;
        // Скрытое поле для настроения (по умолчанию 10)
        int mood = 10;
};

// Описание метода увеличения веса
void Worker::eat (float how_much) {
    if (how_much >= 10) {
        age += 1;
        weight = weight + (how_much / 2);
    }
    else {
        weight = weight + how_much;
    }
}

// Описание геттера веса
float Worker::get_weight() {
    return weight;
}

// Описание сеттера веса
void Worker::set_weight(float new_weight) {
    weight = new_weight;
}

// Описание методов для изменения настроения:

// Метод "гулять" повышает настроение на 1
void Worker::walk() {
    mood += 1;
}

// Метод "танцевать" повышает настроение на 2
void Worker::dance() {
    mood += 2;
}

// Метод "работать" понижает настроение на 2
void Worker::work() {
    mood -= 2;
    mood = (mood > 0) ? mood : 0;
}

// Геттер настроения
int Worker::get_mood() {
    return mood;
}

int main() {
    // Задание русской локали для вывода кириллицы
    setlocale(LC_ALL, "Russian");
    
    //Кириллица в Windows
    //SetConsoleCP(1251);
    //SetConsoleOutputCP(1251);
    
    // Создание объекта класса Worker
    Worker *wrk1 = new Worker();
    
    // Заполнение полей объекта
    std::cout << "Возраст: ";
    std::cin >> wrk1 -> age;
    std::cout << "Имя: ";
    std::cin >> wrk1 -> name;
    std::cout << std::endl;
    
    // Вывод информации о public-полях объекта
    wrk1 -> print_info();
    
    // Нельзя записать что-либо в поле weight напрямую
    // из-за модификатора доступа private
    // wrk1 -> weigth = 70;
    
    // Запись значения с помощью сеттера
    wrk1 -> set_weight(70);
    
    // Заставляем рабочего съесть 2, затем 3 кг пищи, смотрим итог
    wrk1 -> eat(2);
    wrk1 -> eat(3);
    float ves;
    ves = wrk1 -> get_weight();
    std::cout << "\nВес рабочего: " << ves << ".\n\n";
     
    // Усовершенствованный eat, когда при съедании более 10 кг еды
    // Возраст увеличивается на единицу, вес - на половину от съеденного
    wrk1 -> eat(15);
    ves = wrk1 -> get_weight();
    wrk1 -> print_info();
    std::cout << "Вес рабочего: " << ves << ".\n";
    
    // После еды рабочий дважды гуляет и трижды танцует
    wrk1 -> walk();
    wrk1 -> walk();
    wrk1 -> dance();
    wrk1 -> dance();
    wrk1 -> dance();
    
    // Рабочий в цикле работает 9 раз (особенно второй)
    for (int i = 0; i < 9; i++) {
        wrk1 -> work();
    }
    
    // Выводим настроение рабочего
    std::cout << "\nНастроение: " << wrk1 -> get_mood() << "\n\n";
    
    return 0;
}

