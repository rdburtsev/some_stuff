﻿// Лабораторная работа №2
// Вариант 2. Если длина L четная, то удаляются 2 первых и 2 последних символа;

#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <cctype>
#include <cstring>
#include <fstream>
#include "Windows.h"

//Подключение пространства имен
using namespace std;

//Создание основного класса
class MyString
{
private:
	char* str; //вводимая динамическая строка

public:
	MyString(); //конструктор по умолчанию
	MyString(const char* s); //конструктор с параметром
	MyString(const MyString& oblect); //конструктор копирования
	~MyString(); //деструктор

	void set(char* s); //метод ввода строки
	void update(); //метода обработки строки
	void print(); //метод вывода обработаннной строки

};

MyString::MyString()
{
	str = new char[1];
	str[0] = '\0';
	cout << "Сработал конструктор по умолчанию!\n";
}

MyString::MyString(const char* s)
{
	str = new char[strlen(s) + 1];
	strcpy(str, s);

	cout << "Сработал конструктор с параметром!\n";
}

MyString::MyString(const MyString& object)
{
	str = new char[strlen(object.str) + 1];
	strcpy(str, object.str);

	cout << "Сработал конструктор копирования!\n";
}

MyString::~MyString(void)
{
	delete[] str;

	cout << "Сработал деструктор!\n";
}

void MyString::set(char* s)
{
	str = new char[strlen(s) + 1];
	strcpy(str, s);

	cout << "Сработал метод ввода строки!\n";
}

// Если длина L четная, то удаляются 2 первых и 2 последних символа;
void MyString::update()
{
	if (strlen(str) <= 4) {
		cout << "Строка слишком короткая!\n";
	}
	else if (strlen(str) % 2 != 0) {
		cout << "Длина строки - нечетное число!\n";
	}
	else {

		ofstream fout("MyString.txt");
		fout << "Исходная строка: " << str << '\n';
		int length = strlen(str) + 1;
		char* new_str = new char[length-4];
		for (unsigned int i = 0; i < length - 4 - 1; i++) {
			new_str[i] = str[i + 2];
		}
		new_str[length - 4 - 1] = '\0';
		delete[] str;
		str = new_str;

		fout << "Новая строка:      " << str << '\n';
		fout.close();
	}
}

void MyString::print()
{
	cout << str << '\n';
	cout << "Метод печати строки сработал!\n";
}


int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	cout << "Создание обЪекта...\n";
	MyString S1;
	char str[1024];
	cout << "Введите строку: \n";
	cin.getline(str, 1024);
	S1.set(str);
	S1.update();
	S1.print();
}
