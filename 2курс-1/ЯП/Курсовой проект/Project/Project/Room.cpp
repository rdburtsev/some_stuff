#include "Room.h"

Room::Room(size_t _size) {
	Room::size = _size;
}

Room::~Room() {
	delete[] Room::List;
}

void Room::set_size(size_t _size) {
	if (_size >= occupied) {
		Room::size = _size;
	}
	else {
		throw "���������� �������� ������: � ������� ������ �������, ��� � �����������!\0";
	}
}

size_t Room::get_size() {
	return Room::size;
}

size_t Room::get_space() {
	return Room::size - Room::occupied;
}

void Room::clear() {
	delete[] Room::List;
	Room::size = 0;
	Room::occupied = 0;
}

void Room::push(Child *C) {
	if (Room::get_space()) {
		Room::occupied += 1;
		Child** new_List = new Child * [occupied];

		for (size_t i = 0; i < occupied; i++) {
			if (i == occupied - 1) {
				new_List[i] = C;
			}
			else {
				new_List[i] = Room::List[i];
			}
		}

		delete[] Room::List;
		Room::List = new_List;
	}
	else {
		throw "���������� ��������: � ������� ��� �����!\0";
	}
}

void Room::pop(size_t index) {
	if (index < Room::occupied) {
		Room::occupied -= 1;
		Child** new_List = new Child * [occupied];

		for (size_t i = 0; i < occupied; i++) {
			if (i < index) {
				new_List[i] = Room::List[i];
			}
			else {
				new_List[i] = Room::List[i + 1];
			}
		}

		delete[] Room::List;
		Room::List = new_List;
	}
	else {
		throw "��������� ����� ������ ����� �������!\0";
	}
}

Child* Room::operator[](size_t index) {
	if (index < occupied) {
		return Room::List[index];
	}
	else {
		throw "������������ ������!\0";
	}
}