#pragma once

#include <iostream>
#include <string>

#include "SimpleDB.h"
#include "Child.h"
#include "ChildList.h"
#include "Room.h"
#include "TUI.h"

/*
	��������� �������������� ������ ������������ ����������
	��������� �������������� ������������ :
	-����������� ��� ������;
	-��������� ������(������), ��������� � �������������� �������;
	-��������� ����� ������;
	-��������� ���������� �� ��������� ����� � ���������� ������� / � ������� ��������.
*/

class UserUI
{
	public:
		void main_loop(std::string _PATH);

		void show_all_data();

		void show_room();
		
		void search();

		void sort();
};

