#pragma once

#include <string>
#include <cctype>
#include <sstream>

// ����� ��� �������� ������ �����, ����������� � ������

//	������ ������ � �����:
// �������
// ���
// ��������
// ������
// ������� ����
// ������� � ������������ ������������
// ����� �� ������ ����� �����
// ����� �������

enum field : int { f = 0, i, o, group, mark, activity, income, room, end };

class Child
{
	public:
		Child();
		Child(std::string _F,
              std::string _I,
              std::string _O,
              std::string _GROUP,
              std::string _MARK,
              std::string _ACTIVITY,
              std::string _INCOME,
              std::string _ROOM);

		std::string  get_F();
		std::string  get_I();
		std::string  get_O();
		std::string  get_GROUP();
		double       get_MARK();
		std::string  get_ACTIVITY();
		unsigned int get_INCOME();
		unsigned int get_ROOM();
		
		void         set_F(std::string _F);
		void         set_I(std::string _I);
		void         set_O(std::string _O);
		void         set_GROUP(std::string _GROUP);
		void         set_MARK(double _MARK);              void set_MARK(std::string _MARK);
		void         set_ACTIVITY(std::string _ACTIVITY);
		void         set_INCOME(unsigned int _INCOME);    void set_INCOME(std::string _INCOME);
		void         set_ROOM(unsigned int _ROOM);        void set_ROOM(std::string _ROOM);

		std::string operator [](const field F);

		unsigned int get_ID();
		void set_ID(unsigned int _ID);

		Child* ptr();

	private:
		std::string  F;
		std::string  I;
		std::string  O;
		std::string  GROUP = "000000";
		double       MARK = 0;
		std::string  ACTIVITY;
		unsigned int INCOME = 0;
		unsigned int ROOM = 0;

		unsigned int ID = 0;
};

