#include "SimpleDB.h"

SimpleDB::SimpleDB(std::string _PATH) {
	SimpleDB::PATH = _PATH;
}

void SimpleDB::read_file() {
	try {
		std::ifstream fin(PATH);
		std::vector <std::string> INPUT;
		std::string ROW;
		while (getline(fin, ROW)) {
			INPUT.push_back(ROW);
		}
		fin.close();
		ROWS = INPUT;
	}
	catch (...) {
		throw "������ ������ �����!\0";
	}
}

void SimpleDB::write_file() {
	try {
		std::ofstream fout(PATH);
		for (unsigned int i = 0; i < ROWS.size(); i++) {
			fout << ROWS[i] << std::endl;
		}
		fout.close();
	}
	catch (...) {
		throw "������ ������ � ����!\0";
	}
}

void SimpleDB::clear() {
	ROWS.clear();
}

void SimpleDB::push(std::string row) {
	ROWS.push_back(row);
}

bool SimpleDB::is_open() {
	std::fstream file(SimpleDB::PATH);
	return file.is_open();
}

size_t SimpleDB::size() const {
	return ROWS.size();
}

std::string SimpleDB::operator[](size_t index) const {
	return ROWS[index];
}