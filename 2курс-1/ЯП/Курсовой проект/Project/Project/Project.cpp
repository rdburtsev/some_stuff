﻿// Project.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include "Windows.h"
#include "SimpleDB.h"
#include "Child.h"
#include "TUI.h"

#include "UserUI.h"

int main()
{
    system("color 0E");
    UserUI UI;
    UI.main_loop("DataBase.txt");

    return 0;
}