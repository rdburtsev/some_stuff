#pragma once

#include <string>
#include <vector>
#include "Child.h"

class ChildList
{
	public:
		ChildList();
		ChildList(const ChildList& CList);
		~ChildList();

		void push(Child C);
		void pop(unsigned int index);
		unsigned int length();

		ChildList operator=(const ChildList &CList);
		Child operator[](unsigned int index) const;

		ChildList sort(field Field) const;

		ChildList search(std::string word, field Field = end) const;

		void reset_ID();

	private:
		Child* List;
		unsigned int size = 0;
};