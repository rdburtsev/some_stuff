#include "ChildList.h"

ChildList::ChildList() {
	ChildList::List = new Child[0];
}

ChildList::ChildList(const ChildList& CList) {
	ChildList::size = CList.size;
	ChildList::List = new Child[size];
	for (unsigned int i = 0; i < size; i++) {
		ChildList::List[i] = CList.List[i];
	}
}

ChildList::~ChildList() {
	delete[] List;
}

void ChildList::push(Child C) {
	size++;
	Child* new_List = new Child[size];
	for (unsigned int i = 0; i < size; i++) {
		if (i < size - 1) {
			new_List[i] = List[i];
		}
		else {
			new_List[i] = C;
		}
	}
	delete[] List;
	List = new_List;
}

void ChildList::pop(unsigned int index) {
	if (index < size) {
		size--;
		Child* new_List = new Child[size];
		for (unsigned int i = 0; i < size; i++) {
			if (i < index) {
				new_List[i] = List[i];
			}
			else {
				new_List[i] = List[i + 1];
			}
		}
		delete[] List;
		List = new_List;
	}
	else {
		throw "������������ ��������: ������ ������ ������� ������!\0";
	}
}

unsigned int ChildList::length() {
	return size;
}

ChildList ChildList::operator=(const ChildList& CList) {
	ChildList result;
	result.size = CList.size;
	result.List = new Child[size];
	for (unsigned int i = 0; i < size; i++) {
		result.List[i] = CList.List[i];
	}
	return result;
}

Child ChildList::operator[](unsigned int index) const {
	if (index < ChildList::size) {
		return ChildList::List[index];
	}
	else {
		throw "������������ ��������: ������ ������ ������� ������!\0";
	}
}

ChildList ChildList::sort(field Field) const {
	ChildList result = (*this);
	for (unsigned int i = 0; i < result.size - 1; i++) {
		for (unsigned int j = i + 1; j < result.size; j++) {
			if (result.List[j][Field] < result.List[i][Field]) {
				Child bufer = List[j];
				List[j] = List[i];
				List[i] = bufer;
			}
		}
	}
	return result;
}

ChildList ChildList::search(std::string word, field Field) const {
	ChildList result;
	if (Field == end) {
		for (unsigned int i = 0; i < size; i++) {
			for (unsigned int j = 0; (field)j < end; j++) {
				if (List[i][(field)j].find(word) != -1) {
					result.push(List[i]);
					break;
				}
			}
		}
	}
	else {
		for (unsigned int i = 0; i < size; i++) {
			if (List[i][Field].find(word) != -1) {
				result.push(List[i]);
				break;
			}
		}
	}
	return result;
}

void ChildList::reset_ID() {
	for (unsigned int i = 0; i < ChildList::size; i++) {
		ChildList::List[i].set_ID(i);
	}
}