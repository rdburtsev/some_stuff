#pragma once

#include "Child.h"

#define ROOM 11
#define SPACE 6

class Room
{
	public:
		Room(size_t _size = -1);
		~Room();

		void set_size(size_t _size = -1);
		size_t get_size();
		size_t get_space();

		void clear();
		void push(Child *C);
		void pop(size_t index);

		Child* operator[](size_t index);

	private:
		Child** List = NULL;
		size_t size;
		size_t occupied = 0;
		
};

