#include "Child.h"

Child::Child() {
	// ctor
}

Child::Child(std::string _F,
             std::string _I,
             std::string _O,
             std::string _GROUP,
             std::string _MARK,
             std::string _ACTIVITY,
             std::string _INCOME,
             std::string _ROOM) {

	Child::set_F(_F);
	Child::set_I(_I);
	Child::set_O(_O);
	Child::set_GROUP(_GROUP);
	Child::set_MARK(_MARK);
	Child::set_ACTIVITY(_ACTIVITY);
	Child::set_INCOME(_INCOME);
	Child::set_ROOM(_ROOM);
}

std::string  Child::get_F() {
	return Child::F;
}
std::string  Child::get_I() {
	return Child::I;
}
std::string  Child::get_O() {
	return Child::O;
}
std::string  Child::get_GROUP() {
	return Child::GROUP;
}
double       Child::get_MARK() {
	return Child::MARK;
}
std::string  Child::get_ACTIVITY() {
	return Child::ACTIVITY;
}
unsigned int Child::get_INCOME() {
	return Child::INCOME;
}
unsigned int Child::get_ROOM() {
	return Child::ROOM;
}

void         Child::set_F(std::string _F) {
	Child::F = _F;
}
void         Child::set_I(std::string _I) {
	Child::I = _I;
}
void         Child::set_O(std::string _O) {
	Child::O = _O;
}
void         Child::set_GROUP(std::string _GROUP) {
	if (_GROUP.size() != 6) {
		throw "����� ������ ������ ������ ���� ����� 6!\0";
	}
	else {
		bool haveNotADigit = false;
		for (int i = 0; i < _GROUP.size(); i++) {
			if (!isdigit((unsigned char) _GROUP[i])) {
				haveNotADigit = true;
				break;
			}
		}
		if (haveNotADigit) {
			throw "����� ������ ������ �������� ������ �� ����!\0";
		}
		else {
			Child::GROUP = _GROUP;
		}
	}
}
void         Child::set_MARK(double _MARK) {
	if (_MARK < 2.0) {
		throw "������� ��������� ������� ����!\0";
	}
	else if (_MARK > 5.0) {
		throw "������� ������� ������� ����!\0";
	}
	else {
		Child::MARK = _MARK;
	}
}

//
void Child::set_MARK(std::string _MARK) {
	Child::set_MARK(atof(_MARK.c_str()));
}

void         Child::set_ACTIVITY(std::string _ACTIVITY) {
	Child::ACTIVITY = _ACTIVITY;
}
void         Child::set_INCOME(unsigned int _INCOME) {
	if (_INCOME > 0) {
		Child::INCOME = _INCOME;
	}
	else {
		throw "����� ������ ���� ������������� ������!\0";
	}
}

//
void Child::set_INCOME(std::string _INCOME) {
	Child::set_INCOME((unsigned int)atoi(_INCOME.c_str()));
}

void         Child::set_ROOM(unsigned int _ROOM) {
		Child::ROOM = _ROOM;
}

//
void Child::set_ROOM(std::string _ROOM) {
	Child::set_ROOM((unsigned int)atoi(_ROOM.c_str()));
}




std::string Child::operator [](const field F) {	
	switch (F) {
	case(f):
		return Child::get_F();
		break;
	case(i):
		return Child::get_I();
		break;
	case(o):
		return Child::get_O();
		break;
	case(group):
		return Child::GROUP;
		break;
	case(mark): {
			// ������� �������������� � ������ 
			std::stringstream os;
			os.precision(2);
			os << std::fixed << Child::get_MARK();
			return os.str();
		}
		break;
	case(activity):
		return Child::get_ACTIVITY();
		break;
	case(income): {
			// ������� �������������� � ������ 
			std::stringstream os;
			os << Child::get_INCOME();
			return os.str();
		}
		break;
	case(room): {
			// ������� �������������� � ������ 
			std::stringstream os;
			os << Child::get_ROOM();
			return os.str();
		}
		break;
	default:
		throw "������: ������� �������� ����!\0";
	}
}

unsigned int Child::get_ID() {
	return Child::ID;
}

void Child::set_ID(unsigned int _ID) {
	Child::ID = _ID;
}

Child* Child::ptr() {
	return this;
}