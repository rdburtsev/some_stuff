#include "TUI.h"

TUI::TUI() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
}

void TUI::print_866(unsigned int c) {
	SetConsoleCP(866);
	SetConsoleOutputCP(866);
	printf("%c", c);
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
}

void TUI::print(unsigned int c) {
	printf("%c", c);
}

void TUI::set_CP(unsigned int CP) {
	SetConsoleCP(CP);
	SetConsoleOutputCP(CP);
}

void TUI::clear() {
	system("CLS");
}

void TUI::bell() {
	print_866(7);
}

void TUI::print_Child(Child C) {
	// 1 ������
	printf("\n ");
	set_CP(866);
	print(201);
	for (int i = 0; i < 4; i++) {
		print(205);
	}
	print(203);
	for (int i = 0; i < 9; i++) {
		print(205);
	}
	print(203);
	for (int i = 0; i < 19; i++) {
		print(205);
	}
	print(187);
	set_CP(1251);
	printf("\n");
	
	// 2 ������
	printf(" ");
	print_866(186);
	printf(" ID ");
	print_866(186);
	printf(" %7d ", C.get_ID() + 1);
	print_866(186);
	printf(" ������� ");
	if (C[room] != "0") {
		printf("%-9s ", C[room].c_str());
	}
	else {
		printf("�� ������ ");
	}
	print_866(186);
	printf("\n");
	
	// 3 ������
	printf(" ");
	set_CP(866);
	print(204);
	for (int i = 0; i < 4; i++) {
		print(205);
	}
	print(202);
	for (int i = 0; i < 9; i++) {
		print(205);
	}
	print(202);
	for (int i = 0; i < 7; i++) {
		print(205);
	}
	print(209);
	for (int i = 0; i < 11; i++) {
		print(205);
	}
	print(202);
	for (int i = 0; i < 80; i++) {
		print(205);
	}
	print(187);
	set_CP(1251);
	printf("\n");

	// 4 ������
	printf(" ");
	print_866(186);
	printf(" �������              ");
	print_866(179);
	std::string s = C[f];
	if (s.size() > 90) {
		s = s.erase(87) + "...";
	}
	printf(" %-90s ", s.c_str());
	print_866(186);
	printf("\n");

	// 5 ������
	printf(" ");
	set_CP(866);
	print(199);
	for (int i = 0; i < 22; i++) {
		print(196);
	}
	print(197);
	for (int i = 0; i < 92; i++) {
		print(196);
	}
	print(182);
	set_CP(1251);
	printf("\n");

	// 6 ������
	printf(" ");
	print_866(186);
	printf(" ���                  ");
	print_866(179);
	s = C[i];
	if (s.size() > 90) {
		s = s.erase(87) + "...";
	}
	printf(" %-90s ", s.c_str());
	print_866(186);
	set_CP(1251);
	printf("\n");

	// 7 ������
	printf(" ");
	set_CP(866);
	print(199);
	for (int i = 0; i < 22; i++) {
		print(196);
	}
	print(197);
	for (int i = 0; i < 92; i++) {
		print(196);
	}
	print(182);
	set_CP(1251);
	printf("\n");

	// 8 ������
	printf(" ");
	print_866(186);
	printf(" ��������             ");
	print_866(179);
	s = C[o];
	if (s.size() > 90) {
		s = s.erase(87) + "...";
	}
	printf(" %-90s ", s.c_str());
	print_866(186);
	printf("\n");

	// 9 ������
	printf(" ");
	set_CP(866);
	print(199);
	for (int i = 0; i < 22; i++) {
		print(196);
	}
	print(197);
	for (int i = 0; i < 92; i++) {
		print(196);
	}
	print(182);
	set_CP(1251);
	printf("\n");

	// 10 ������
	printf(" ");
	print_866(186);
	printf(" ������               ");
	print_866(179);
	s = C[group];
	if (s.size() > 90) {
		s = s.erase(87) + "...";
	}
	printf(" %-90s ", s.c_str());
	print_866(186);
	printf("\n");

	// 11 ������
	printf(" ");
	set_CP(866);
	print(199);
	for (int i = 0; i < 22; i++) {
		print(196);
	}
	print(197);
	for (int i = 0; i < 92; i++) {
		print(196);
	}
	print(182);
	set_CP(1251);
	printf("\n");

	// 12 ������
	printf(" ");
	print_866(186);
	printf(" ������� ����         ");
	print_866(179);
	s = C[mark];
	if (s.size() > 90) {
		s = s.erase(87) + "...";
	}
	printf(" %-90s ", s.c_str());
	print_866(186);
	printf("\n");

	// 13 ������
	printf(" ");
	set_CP(866);
	print(199);
	for (int i = 0; i < 22; i++) {
		print(196);
	}
	print(197);
	for (int i = 0; i < 92; i++) {
		print(196);
	}
	print(182);
	set_CP(1251);
	printf("\n");

	// 14 ������
	printf(" ");
	print_866(186);
	printf(" ����� �� ����� ����� ");
	print_866(179);
	s = C[income];
	if (s.size() > 90) {
		s = s.erase(87) + "...";
	}
	printf(" %-90s ", s.c_str());
	print_866(186);
	printf("\n");

	// 15 ������
	printf(" ");
	set_CP(866);
	print(204);
	for (int i = 0; i < 22; i++) {
		print(205);
	}
	print(207);
	for (int i = 0; i < 92; i++) {
		print(205);
	}
	print(185);
	set_CP(1251);
	printf("\n");

	// 16 ������
	printf(" ");
	print_866(186);
	printf("                                        ������� � ������������ ������������                                        ");
	print_866(186);
	printf("\n");

	// 17 ������
	printf(" ");
	set_CP(866);
	print(199);
	for (int i = 0; i < 115; i++) {
		print(196);
	}
	print(182);
	set_CP(1251);
	printf("\n");

	// 18 ������
	printf(" ");
	print_866(186);
	printf("%115s", "");
	print_866(186);
	printf("\n");

	// 19 ������
	printf(" ");
	print_866(186);
	s = C[activity];
	if (s.size() > 113) {
		s = s.erase(110) + "...";
	}
	printf(" %-113s ", s.c_str());
	print_866(186);
	printf("\n");

	// 20 ������
	printf(" ");
	print_866(186);
	printf("%115s", "");
	print_866(186);
	printf("\n");

	// 21 ������
	printf(" ");
	set_CP(866);
	print(200);
	for (int i = 0; i < 115; i++) {
		print(205);
	}
	print(188);
	set_CP(1251);
	printf("\n");
	printf("\n");

}