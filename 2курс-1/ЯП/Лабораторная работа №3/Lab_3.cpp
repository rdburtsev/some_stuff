// Лабораторная работа №3
// Бурцев Роман, КБ-211
// Подгруппа 1, вариант 2

/*
    Вариант 2.
    Класс Треугольник
    Свойства: три стороны
    Операции:
    - увеличение/уменьшение размера сторон на заданное количество процентов;
    - вычисление средней линии для любой из сторон;
    - определение вида треугольника по величине углов (Остроугольный, Тупоугольный,
      Прямоугольный);
    - определение значений углов.
    
		ЗАДАНИЕ 1. Статические члены-данные класса
	1. Добавить статическое поле int сount, выступающее в роли счетчика
	объектов класса.
	2. Деструктор класса должен уменьшать на единицу значение счетчика.
	3. Добавить статический метод int getCount() возвращающий значение
	счетчика.
	4. Продемонстрировать изменение значения статического поля.

		ЗАДАНИЕ 2 Константные методы и объекты
	1. Определить какие методы являются константными, определить
	константные параметры и константные возвращаемые значения
	методов.
	2. Добавить константное поле, хранящее идентификатор объекта (номер
	созданного объекта), предусмотреть методы вывода информации о
	идентификаторе.
	3. Описать и инициализировать обычные и константные объекты.
	4. Выполнить вызовы обычных и константных методов для каждого вида
	объектов.
	5. Провести тестирование программы: Откомпилировать программу.
	Имеются ли ошибки компиляции и какие? Если имеются, то
	закомментировать соответствующие строки кода и вновь провести
	компиляцию. Какие предупреждения выдает компилятор и в чем их
	смысл? Как их можно объяснить с позиции обеспечения надежности
	программы?
*/

#define PI 3.14159265358979323846

#include <iostream>
#include <math.h>
#include <string>

class Triangle {
	
	public:
		// Конструктор
		Triangle();
		
		// Деструктор
		~Triangle();
		
		// Сеттер для сторон
		bool setSide(int numberOfSide, double length);
		
		// Геттер для сторон
		double getSide(int numberOfSide) const;
		
		// Вывод информации о длинах сторон
		void show() const;
		
		// Изменение размера всех сторон на заданное число процентов
		bool resize(double percent);
		
		// Вычисление средней линии для любой из сторон
		double middleLine(int numberOfSide) const;
		
		// Определение типа треугольника
		bool getType();
		
		// Определение величин углов
		double getAngle(int numberOfAngle);
		
		// Геттер и инициализатор счетчика объектов
		static int getCount();
		
		// Возвращение ID треугольника
		int getID() const;
		
	private:
		// Длины сторон
		double sides[3] = { 0 };
		
		// Квадрат числа для упрощения записи методов
		double sqr(double x);
		
		// Счетчик объектов
		static int Count;
		
		// ID треугольника
		const int ID = Count;
};

int Triangle::Count = 0;

int main() {
	setlocale(LC_ALL, "Russian");
	
	{
		Triangle array[7];
	}
	
	{
		Triangle t1;
		const Triangle t2;
		
		std::cout << "----------\n";
		
		t1.setSide(0, t1.getSide(0));
		t1.getSide(0);
		t1.show();
		t1.resize(100);
		t1.middleLine(0);
		t1.getType();
		t1.getAngle(0);
		t1.getCount();
		t1.getID();
		
		std::cout << "----------\n";
		
		//t2.setSide(int numberOfSide, double length);
		t2.getSide(0);
		t2.show();
		//t2.resize(double percent);
		t2.middleLine(0);
		//t2.getType();
		//t2.getAngle(0);
		//t2.getCount();
		t2.getID();
		
		std::cout << "----------\n";
	}
	
    system("pause");
	return 0;
}
/*
Triangle::Triangle() {
	std::cout << "\nЗадайте исходные свойства треугольника\n";
    bool existence = false;
    do {
        for (int i = 0; i < 3; i++) {
            bool condition = false;
            while (!condition) {
                std::cout << "Введите длину cтороны " << i+1 << ":\n>>>";
                double value;
                std::cin >> value;
                condition = this -> setSide(i, value);
            }
            condition = false;
        }
        existence = this -> getType();
    } while (!existence);
    Count++;
    std::cout << "Трегольников создано: " << getCount() << '\n';
}
*/

Triangle::Triangle() {
	std::cout << "\nСоздание треугольника со сторонами 3, 4, 5...\n";
    Triangle::setSide(0, 3);
    Triangle::setSide(1, 4);
    Triangle::setSide(2, 5);
    Count++;
    std::cout << "Трегольников создано: " << getCount() << '\n';
}

// Сеттер для сторон
bool Triangle::setSide(int numberOfSide, double length) {
    if (length > 0) {
        sides[numberOfSide % 3] = length;
        return true;
    } else {
        std::cout << "Сторона не может иметь неположительную длину!\n";
        return false;
    }
}

// Геттер для сторон
double Triangle::getSide(int numberOfSide) const {
	return sides[numberOfSide % 3];
}

// Вывод информации о длинах сторон
void Triangle::show() const {
	printf("Треугольник имеет стороны длиной %0.2lf, %0.2lf, %0.2lf\n", sides[0], sides[1], sides[2]);
}

// Изменение размера всех сторон на заданное число процентов
bool Triangle::resize(double percent) {
	if (percent <= -100.0) {
		std::cout << "Невозможно выполнить операцию!\n";
		return false;
	}
	else {
		for (int i = 0; i < 3; i++) {
			sides[i] *= (100.0 + percent) * 0.01;
		}
		return true;
	}
}

// Вычисление средней линии для любой из сторон
double Triangle::middleLine(int numberOfSide) const {
	return sides[ --numberOfSide % 3 ] / 2.0;
}

// Определение типа треугольника
bool Triangle::getType() {
		if ((sides[0] < sides[1] + sides[2]) &&
            (sides[1] < sides[0] + sides[2]) &&
            (sides[2] < sides[0] + sides[1]))
        {
            int max = 0, min = 0, middle;
            // Маленькая манипуляция с индексами сторон в массиве
            for (int i = 0; i < 3; i++) {
                if (sides[i] > sides[max])
                    max = i;
                if (sides[i] <= sides[min])
                    min = i;
            }
            middle = 3 - max - min;
            // Определение типа треугольника
            if (sqr(sides[max]) == sqr(sides[middle]) + sqr(sides[min])) {
                std::cout << "Треугольник прямоугольный!\n";
            }
            else if (sqr(sides[max]) > sqr(sides[middle]) + sqr(sides[min])) {
                std::cout << "Прямоугольник тупоугольный!\n";
            } else
                std::cout << "Прямоугольник остроугольный!\n";
            return true;
        }
        else {
            std::cout << "Треугольника с такими сторонами не существует!\n";
            return false;
        }
}

// Определение величин углов
double Triangle::getAngle(int numberOfAngle) {
	int n = --numberOfAngle;
	 // cos Alpha = (c^2 + b^2 - a^2) / 2cb
    return acos((sqr(sides[(n+2)%3]) + sqr(sides[(n+1)%3]) - sqr(sides[n%3])) /
		   (2*sides[(n+1)%3]*sides[(n+2)%3])) * 180.0 / PI;
}

// Квадрат числа
double Triangle::sqr(double x) {
	return x * x;
}

// Количество треугольников
int Triangle::getCount() {
	return Count;
}

// Деструктор
Triangle::~Triangle() {
	Count--;
	std::cout << "Уничтожение треугольника "<< ID << "\nТреугольников осталось: " << Count << '\n';
}

// Идентификатор треугольника
int Triangle::getID() const {
	return ID;
}
