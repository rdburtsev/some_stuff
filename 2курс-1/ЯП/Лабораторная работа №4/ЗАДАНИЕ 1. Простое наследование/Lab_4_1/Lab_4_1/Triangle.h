#pragma once
#define _USE_MATH_DEFINES
#include <iostream>
#include <cmath>


class Triangle
{
public:
    Triangle(double a = 1, double b = 1, double c = 1);  // ����������� � �����������: a, b, c - ������� ������������


    bool is_exist();                                     // �������� �������������
    void show();                                         // ����� ������, ����, ��������, �������

    double get_a();                                      // ������� ������
    double get_b();
    double get_c();
    double get_area();                                   // ������ �������

protected:
    double a, b, c;                                      // ������� ������������

    double sqr(double x) const;                          // ������� ��������
};


