﻿#include <iostream>
#include <Windows.h>

#include "triangle.h"
#include "etriangle.h"
/*
        2. Создать класс треугольник, члены класса – длины 3-х сторон.
    Предусмотреть в классе методы проверки существования треугольника,
    вычисления и вывода сведений о фигуре – длины сторон, углы, периметр,
    площадь. Создать производный класс – равносторонний треугольник,
    перегрузить в классе проверку, является ли треугольник равносторонним и
    метод вывода сведений о фигуре. Написать программу, демонстрирующую
    работу с классом: дано K треугольников и L равносторонних треугольников,
    найти среднюю площадь для K треугольников и наибольший равносторонний
    треугольник.
*/

#define K 6
#define L 5

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    try
    {
        {
            double area = 0;
            Triangle T[K];
            for (unsigned int i = 0; i < K; i++) {
                unsigned int j = i + 1;
                T[i] = Triangle(j, 1.5 * j, 2 * j);
                printf("Треугольник %d:\n", j);
                T[i].show();
                area += T[i].get_area();
            }
            printf("Средняя площадь: %0.3lf\n\n", area / K);
        }
        for (unsigned int i = 0; i < 120 - 1; i++) { printf("-"); }
        printf("\n\n");
        {
            unsigned int max = 0;
            ETriangle ET[L];
            for (unsigned int i = 0; i < L; i++) {
                unsigned int j = i + 1;
                ET[i] = ETriangle(j);
                printf("Равносторонний треугольник %d:\n", j);
                ET[i].show();
                max = (ET[i].get_a() > ET[max].get_a()) ? i : max;
            }
            printf("Наибольший треугольник имеет номер %d\n\n", max + 1);
        }
    }
    catch (const char* msg)
    {
        printf("%s\n", msg);
    }
    return 0;
}