#include "triangle.h"

// ����������� � �����������: a, b, c - ������� ������������
Triangle::Triangle(double a, double b, double c)
{
    Triangle::a = a;
    Triangle::b = b;
    Triangle::c = c;
}

// �������� �������������
bool Triangle::is_exist()
{
    if ((a < b + c) && (b < a + c) && (c < a + b) &&
        (a > 0) && (b > 0) && (c > 0))
        return true;
    else return false;
}

// ����� ������, ����, ��������, �������
void Triangle::show()
{
    if (!Triangle::is_exist()) {
        throw "Triangle. ���������� ���������: ������������ �� ����������!";
    }
    else {
        double alpha, beta, gamma;
        alpha = acos((sqr(b) + sqr(c) - sqr(a)) / (2 * b * c));
        beta = acos((sqr(a) + sqr(c) - sqr(b)) / (2 * a * c));
        gamma = acos((sqr(a) + sqr(b) - sqr(c)) / (2 * a * b));
        double perimeter, area;
        perimeter = a + b + c;
        area = 0.5 * sin(alpha) * b * c;
        printf("%-20s    %-20s    %-20s\n", "�������:", "���� (� ��������):", "��������:");
        printf("%20.4lf    %20.4lf    %20.4lf\n", a, alpha * 180 / M_PI, perimeter);
        printf("%20.4lf    %20.4lf    %-20s\n", b, beta * 180 / M_PI, "�������:");
        printf("%20.4lf    %20.4lf    %20.4lf\n\n", c, gamma * 180 / M_PI, area);
    }
}

double Triangle::sqr(double x) const
{
    return x * x;
}

double Triangle::get_a() { return a; }
double Triangle::get_b() { return b; }
double Triangle::get_c() { return c; }
double Triangle::get_area() { return 0.5 * sin(acos((sqr(b) + sqr(c) - sqr(a)) / (2 * b * c))) * b * c; }
