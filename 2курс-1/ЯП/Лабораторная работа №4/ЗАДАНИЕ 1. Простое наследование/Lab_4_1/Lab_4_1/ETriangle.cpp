#include "ETriangle.h"

// ����������� � �����������: a, b, c - ������� ������������
ETriangle::ETriangle(double a, double b, double c) : Triangle(a, b, c) {}
// ����������� � ����������:  side    - ����� ���� ������ ��������������� ������������ ������������
ETriangle::ETriangle(double side) : Triangle(side, side, side) {}




// �������� �������������
bool ETriangle::is_exist()
{
    if ((a == b) && (b == c) && (c == a) &&
        (a > 0) && (b > 0) && (c > 0))
        return true;
    else return false;
}

// ����� ������, ����, ��������, �������
void ETriangle::show()
{
    if (!ETriangle::is_exist()) {
        throw "ETriangle. ���������� ���������: ������������ �� ����������!";
    }
    else {
        double alpha;
        alpha = M_PI / 3;


        double perimeter, area;
        perimeter = a * 3;
        area = 0.5 * sin(alpha) * sqr(a);
        printf("%-20s    %-20s    %-20s\n", "�������:", "���� (� ��������):", "��������:");
        printf("%20.4lf    %20.4lf    %20.4lf\n", a, alpha * 180 / M_PI, perimeter);
        printf("%48s%-20s\n", "", "�������:");
        printf("%68.4lf\n\n", area);
    }
}
